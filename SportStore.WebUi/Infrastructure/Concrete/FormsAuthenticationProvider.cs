﻿using System.Web.Security;
using SportStore.WebUi.Infrastructure.Abstract;

namespace SportStore.WebUi.Infrastructure.Concrete
{
    public class FormsAuthenticationProvider : IAuthenticationProvider
    {
        public bool Authenticate(string username, string password)
        {
            var result = FormsAuthentication.Authenticate(username, password);

            if (result)
            {
                FormsAuthentication.SetAuthCookie(username, false);
            }
            return result;
        }
    }
}