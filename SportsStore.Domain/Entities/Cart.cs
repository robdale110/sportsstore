﻿using System.Collections.Generic;
using System.Linq;

namespace SportStore.Domain.Entities
{
    public class Cart
    {
        private List<CartItem> _lines = new List<CartItem>();

        public IEnumerable<CartItem> Lines
        {
            get { return _lines; }
        }

        public void AddItem(Product product, int quantity)
        {
            var cartItem = _lines.FirstOrDefault(p => p.Product.ProductId == product.ProductId);

            if (cartItem == null)
            {
                _lines.Add(new CartItem {Product = product, Quantity = quantity});
            }
            else
            {
                cartItem.Quantity += quantity;
            }
        }
        
        public void RemoveItem(Product product)
        {
            _lines.RemoveAll(l => l.Product.ProductId == product.ProductId);
        }

        public decimal ComputeTotalValue()
        {
            return _lines.Sum(e => e.Product.Price*e.Quantity);
        }

        public void Clear()
        {
            _lines.Clear();
        }
    }
}