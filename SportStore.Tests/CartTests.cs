﻿using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SportStore.Domain.Entities;

namespace SportStore.Tests
{
    [TestClass]
    public class CartTests
    {
        [TestMethod]
        public void Can_Add_NewLines()
        {
            // Arrange
            var product1 = new Product { ProductId = 1, Name = "Product 1" };
            var product2 = new Product { ProductId = 2, Name = "Product 2" };
            var target = new Cart();

            // Act
            target.AddItem(product1, 1);
            target.AddItem(product2, 1);
            var results = target.Lines.ToArray();

            // Assert
            Assert.AreEqual(results.Length, 2);
            Assert.AreEqual(results[0].Product, product1);
            Assert.AreEqual(results[1].Product, product2);
        }

        [TestMethod]
        public void Can_Add_Quantity_For_Existing_Lines()
        {
            // Arrange
            var product1 = new Product { ProductId = 1, Name = "Product 1" };
            var product2 = new Product { ProductId = 2, Name = "Product 2" };
            var target = new Cart();

            // Act
            target.AddItem(product1, 1);
            target.AddItem(product2, 1);
            target.AddItem(product1, 10);
            var results = target.Lines.OrderBy(c => c.Product.ProductId).ToArray();

            // Assert
            Assert.AreEqual(results.Length, 2);
            Assert.AreEqual(results[0].Quantity, 11);
            Assert.AreEqual(results[1].Quantity, 1);
        }

        [TestMethod]
        public void Can_Remove_Line()
        {
            // Arrange
            var product1 = new Product { ProductId = 1, Name = "Product 1" };
            var product2 = new Product { ProductId = 2, Name = "Product 2" };
            var product3 = new Product { ProductId = 3, Name = "Product 3" };
            var target = new Cart();

            // Act
            target.AddItem(product1, 1);
            target.AddItem(product2, 3);
            target.AddItem(product3, 5);
            target.AddItem(product2, 1);
            target.RemoveItem(product2);

            // Assert
            Assert.AreEqual(target.Lines.Count(c => c.Product == product2), 0);
            Assert.AreEqual(target.Lines.Count(), 2);
        }

        [TestMethod]
        public void Calculate_Cart_Total()
        {
            // Arrange
            var product1 = new Product { ProductId = 1, Name = "Product 1", Price = 100M };
            var product2 = new Product { ProductId = 2, Name = "Product 2", Price = 50M };
            var target = new Cart();

            //Act
            target.AddItem(product1, 1);
            target.AddItem(product2, 1);
            target.AddItem(product1, 3);
            var result = target.ComputeTotalValue();

            //Assert
            Assert.AreEqual(result, 450M);
        }

        [TestMethod]
        public void Can_Clear_Contents()
        {
            // Arrange
            var product1 = new Product { ProductId = 1, Name = "Product 1", Price = 100M };
            var product2 = new Product { ProductId = 2, Name = "Product 2", Price = 50M };
            var target = new Cart();

            // Act
            target.AddItem(product1, 1);
            target.AddItem(product2, 1);
            target.Clear();

            // Assert
            Assert.AreEqual(target.Lines.Count(), 0);
        }
    }
}