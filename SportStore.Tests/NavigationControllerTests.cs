﻿using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using SportStore.Domain.Abstract;
using SportStore.Domain.Entities;
using SportStore.WebUi.Controllers;

namespace SportStore.Tests
{
    [TestClass]
    public class NavigationControllerTests
    {
        private static Mock<IProductRepository> GetMockRepository()
        {
            var mock = new Mock<IProductRepository>();
            mock.Setup(m => m.Products).Returns(new[]
            {
                new Product {ProductId = 1, Name = "Product 1", Category = "Apples"},
                new Product {ProductId = 2, Name = "Product 2", Category = "Apples"},
                new Product {ProductId = 3, Name = "Product 3", Category = "Plums"},
                new Product {ProductId = 4, Name = "Product 4", Category = "Oranges"}
            });
            return mock;
        }

        [TestMethod]
        public void Can_Create_Categories()
        {
            // Arrange
            var mock = GetMockRepository();
            var target = new NavigationController(mock.Object);

            // Act
            var results = ((IEnumerable<string>)target.Menu().Model).ToArray();

            //Assert
            Assert.AreEqual(results.Length, 3);
            Assert.AreEqual(results[0], "Apples");
            Assert.AreEqual(results[1], "Oranges");
            Assert.AreEqual(results[2], "Plums");
        }

        [TestMethod]
        public void Indicates_Selected_Category()
        {
            // Arrange
            var mock = GetMockRepository();
            var target = new NavigationController(mock.Object);
            var categoryToSelect = "Apples";

            // Act
            var result = target.Menu(categoryToSelect).ViewBag.SelectedCategory;

            // Assert
            Assert.AreEqual(categoryToSelect, result);
        }
    }
}
