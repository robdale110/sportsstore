﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web.Mvc;
using SportStore.Domain.Abstract;

namespace SportStore.WebUi.Controllers
{
    public partial class NavigationController : Controller
    {
        private IProductRepository _productRepository;

        public NavigationController(IProductRepository productRepository)
        {
            _productRepository = productRepository;
        }

        // GET: Navigation
        public virtual PartialViewResult Menu(string category = null, bool horizontalLayout = false)
        {
            ViewBag.SelectedCategory = category;

            var categories = _productRepository.Products
                                               .Select(x => x.Category)
                                               .Distinct()
                                               .OrderBy(x => x);

            return PartialView("FlexMenu", categories);
        }
    }
}