﻿using System.Linq;
using System.Web.Mvc;
using SportStore.Domain.Abstract;
using SportStore.WebUi.Models;

namespace SportStore.WebUi.Controllers
{
    public partial class ProductController : Controller
    {
        private readonly IProductRepository _productRepository;
        public int PageSize = 3;

        public ProductController(IProductRepository productRepository)
        {
            _productRepository = productRepository;
        }

        public virtual ViewResult List(string category, int page = 1)
        {
            var viewModel = new ProductListViewModel
            {
                Products = _productRepository.Products
                    .Where(p => category == null || p.Category == category)
                    .OrderBy(p => p.ProductId)
                    .Skip((page - 1) * PageSize)
                    .Take(PageSize),
                PagingInfo = new PagingInfo
                {
                    CurrentPage = page,
                    ItemsPerPage = PageSize,
                    TotalItems = category == null ? _productRepository.Products.Count() : _productRepository.Products.Count(e => e.Category == category)
                },
                CurrentCategory = category
            };
            return View(viewModel);
        }

        public virtual FileContentResult GetImage(int productId)
        {
            var product = _productRepository.Products.FirstOrDefault(p => p.ProductId == productId);

            return product != null ? File(product.ImageData, product.ImageMimeType) : null;
        }
    }
}