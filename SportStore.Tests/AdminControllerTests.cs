﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using SportStore.Domain.Abstract;
using SportStore.Domain.Entities;
using SportStore.WebUi.Controllers;

namespace SportStore.Tests
{
    [TestClass]
    public class AdminControllerTests
    {
        private static Mock<IProductRepository> GetMockRepository()
        {
            var mock = new Mock<IProductRepository>();
            mock.Setup(m => m.Products).Returns(new Product[]
            {
                new Product {ProductId = 1, Name = "Product 1"},
                new Product {ProductId = 2, Name = "Product 2"},
                new Product {ProductId = 3, Name = "Product 3"},
            });
            return mock;
        }

        [TestMethod]
        public void Index_Contains_All_Products()
        {
            // Arrange
            var mock = GetMockRepository();
            var target = new AdminController(mock.Object);

            // Act
            var result = ((IEnumerable<Product>)target.Index().ViewData.Model).ToArray();

            // Assert
            Assert.AreEqual("Product 1", result[0].Name);
            Assert.AreEqual("Product 2", result[1].Name);
            Assert.AreEqual("Product 3", result[2].Name);
        }



        [TestMethod]
        public void Can_Edit_Product()
        {
            // Arrange
            var mock = GetMockRepository();
            var target = new AdminController(mock.Object);

            // Act
            var product1 = target.Edit(1).ViewData.Model as Product;
            var product2 = target.Edit(2).ViewData.Model as Product;
            var product3 = target.Edit(3).ViewData.Model as Product;

            // Assert
            Assert.AreEqual(1, product1.ProductId);
            Assert.AreEqual(2, product2.ProductId);
            Assert.AreEqual(3, product3.ProductId);
        }

        [TestMethod]
        public void Cannot_Edit_Nonexistent_Product()
        {
            // Arrange
            var mock = GetMockRepository();
            var target = new AdminController(mock.Object);

            // Act
            var result = (Product)target.Edit(4).ViewData.Model;

            // Assert
            Assert.IsNull(result);
        }

        [TestMethod]
        public void Can_Save_Valid_Changes()
        {
            // Arrange
            var mock = GetMockRepository();
            var target = new AdminController(mock.Object);
            var product = new Product { Name = "Test" };

            // Act
            var result = target.Edit(product);

            // Assert
            mock.Verify(m => m.SaveProduct(product));
            Assert.IsNotInstanceOfType(result, typeof(ViewResult));
        }

        [TestMethod]
        public void Cannot_Save_Invalid_Changes()
        {
            // Arrange
            var mock = GetMockRepository();
            var target = new AdminController(mock.Object);
            var product = new Product { Name = "Test" };
            target.ModelState.AddModelError("error", "error");

            // Act
            var result = target.Edit(product);

            // Assert
            mock.Verify(m => m.SaveProduct(It.IsAny<Product>()), Times.Never());
            Assert.IsInstanceOfType(result, typeof(ViewResult));
        }

        [TestMethod]
        public void Can_Delete_Valid_Product()
        {
            // Arrange
            var mock = GetMockRepository();
            var product = new Product { ProductId = 4, Name = "Test" };
            mock.Object.SaveProduct(product);
            var target = new AdminController(mock.Object);

            // Act
            target.Delete(product.ProductId);

            // Assert
            mock.Verify(m => m.DeleteProduct(product.ProductId));
            Assert.AreEqual(mock.Object.Products.Count(), 3);
        }
    }
}
