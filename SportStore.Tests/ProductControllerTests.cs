﻿using System;
using System.Linq;
using System.Web.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using SportStore.Domain.Abstract;
using SportStore.Domain.Entities;
using SportStore.WebUi.Controllers;
using SportStore.WebUi.HtmlHelpers;
using SportStore.WebUi.Models;

namespace SportStore.Tests
{
    [TestClass]
    public class ProductControllerTests
    {
        private static Mock<IProductRepository> GetMockRepository()
        {
            var mock = new Mock<IProductRepository>();
            mock.Setup(m => m.Products).Returns(new[]
            {
                new Product {ProductId = 1, Name = "Product 1", Category = "Category 1"},
                new Product {ProductId = 2, Name = "Product 2", Category = "Category 2"},
                new Product {ProductId = 3, Name = "Product 3", Category = "Category 1"},
                new Product {ProductId = 4, Name = "Product 4", Category = "Category 2"},
                new Product {ProductId = 5, Name = "Product 5", Category = "Category 3"}
            }.AsQueryable());
            return mock;
        }

        [TestMethod]
        public void Can_Paginate()
        {
            // Arrange
            var mock = GetMockRepository();
            var controller = new ProductController(mock.Object) { PageSize = 3 };

            // Act
            var result = (ProductListViewModel)controller.List(null, 2).Model;

            // Assert
            var productArray = result.Products.ToArray();
            Assert.IsTrue(productArray.Length == 2);
            Assert.AreEqual(productArray[0].Name, "Product 4");
            Assert.AreEqual(productArray[1].Name, "Product 5");
        }

        [TestMethod]
        public void Can_Generate_Page_Links()
        {
            // Arrange
            // We need to define a HTML helper in order to apply the extension method
            HtmlHelper helper = null;
            var pagingInfo = new PagingInfo
            {
                CurrentPage = 2,
                TotalItems = 28,
                ItemsPerPage = 10
            };

            // We also need to set up the delegate using a lambda expression
            Func<int, string> pageUrlDelegate = i => "Page" + i;

            // Act
            var result = helper.PageLinks(pagingInfo, pageUrlDelegate);

            //Assert
            Assert.AreEqual(@"<a class=""btn btn-default"" href=""Page1"">1</a>"
                            + @"<a class=""btn btn-default btn-primary selected"" href=""Page2"">2</a>"
                            + @"<a class=""btn btn-default"" href=""Page3"">3</a>", result.ToString());
        }

        [TestMethod]
        public void Can_Send_Pagination_View_Model()
        {
            // Arrange
            var mock = GetMockRepository();
            var controller = new ProductController(mock.Object) { PageSize = 3 };

            // Act
            var result = (ProductListViewModel)controller.List(null, 2).Model;

            // Assert
            var pagingInfo = result.PagingInfo;
            Assert.AreEqual(pagingInfo.CurrentPage, 2);
            Assert.AreEqual(pagingInfo.ItemsPerPage, 3);
            Assert.AreEqual(pagingInfo.TotalItems, 5);
            Assert.AreEqual(pagingInfo.TotalPages, 2);
        }

        [TestMethod]
        public void Can_Filter_Products()
        {
            // Arrange
            var mock = GetMockRepository();
            var controller = new ProductController(mock.Object) { PageSize = 3 };

            // Act
            var result = ((ProductListViewModel)controller.List("Category 2", 1).Model).Products.ToArray();

            // Assert
            Assert.AreEqual(result.Length, 2);
            Assert.IsTrue(result[0].Name == "Product 2" && result[0].Category == "Category 2");
            Assert.IsTrue(result[1].Name == "Product 4" && result[1].Category == "Category 2");
        }

        [TestMethod]
        public void Generate_Category_Specific_Product_Count()
        {
            // Arrange
            var mock = GetMockRepository();
            var target = new ProductController(mock.Object) { PageSize = 3 };

            // Act
            var result1 = ((ProductListViewModel)target.List("Category 1").Model).PagingInfo.TotalItems;
            var result2 = ((ProductListViewModel)target.List("Category 2").Model).PagingInfo.TotalItems;
            var result3 = ((ProductListViewModel)target.List("Category 3").Model).PagingInfo.TotalItems;
            var resultAll = ((ProductListViewModel)target.List(null).Model).PagingInfo.TotalItems;

            // Assert
            Assert.AreEqual(result1, 2);
            Assert.AreEqual(result2, 2);
            Assert.AreEqual(result3, 1);
            Assert.AreEqual(resultAll, 5);
        }

        [TestMethod]
        public void Can_Retrieve_Image_Data()
        {
            // Arrange
            var product = new Product
            {
                ProductId = 2,
                Name = "Test",
                ImageData = new byte[] { },
                ImageMimeType = "image/png"
            };
            var mock = new Mock<IProductRepository>();
            mock.Setup(m => m.Products).Returns(new[]
            {
                new Product {ProductId = 1, Name = "Product 1", Category = "Category 1"},
                product,
                new Product {ProductId = 3, Name = "Product 3", Category = "Category 1"},
            }.AsQueryable());
            var target = new ProductController(mock.Object);

            // Act
            var result = target.GetImage(2);

            // Assert
            Assert.IsNotNull(result);
            Assert.IsInstanceOfType(result, typeof(FileResult));
            Assert.AreEqual(product.ImageMimeType, result.ContentType);
        }

        [TestMethod]
        public void MyTestMethod()
        {
            // Arrange
            var mock = GetMockRepository();
            var target = new ProductController(mock.Object);

            // Act
            var result = target.GetImage(6);

            // Assert
            Assert.IsNull(result);
        }
    }
}