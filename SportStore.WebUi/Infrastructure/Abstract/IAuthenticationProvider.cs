﻿namespace SportStore.WebUi.Infrastructure.Abstract
{
    public interface IAuthenticationProvider
    {
        bool Authenticate(string username, string password);
    }
}
