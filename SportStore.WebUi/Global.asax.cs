﻿using System.Data.Entity;
using System.Web.Mvc;
using System.Web.Routing;
using SportStore.Domain.Concrete;
using SportStore.Domain.Entities;
using SportStore.WebUi.Infrastructure.Binders;

namespace SportStore.WebUi
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            ModelBinders.Binders.Add(typeof(Cart), new CartModelBinder());
            // HACK: will this stop the backing error
            Database.SetInitializer<SportStoreDbContext>(null);
        }
    }
}
