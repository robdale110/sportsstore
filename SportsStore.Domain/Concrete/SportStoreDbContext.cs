﻿using System.Data.Entity;
using SportStore.Domain.Entities;

namespace SportStore.Domain.Concrete
{
    public class SportStoreDbContext : DbContext
    {
        public DbSet<Product> Products { get; set; }
    }
}