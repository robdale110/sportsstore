﻿using System.Web.Mvc;
using SportStore.WebUi.Infrastructure.Abstract;
using SportStore.WebUi.Models;

namespace SportStore.WebUi.Controllers
{
    public partial class AccountController : Controller
    {
        private IAuthenticationProvider _authenticationProvider;

        public AccountController(IAuthenticationProvider authenticationProvider)
        {
            _authenticationProvider = authenticationProvider;
        }

        public virtual ViewResult Login()
        {
            return View();
        }

        [HttpPost]
        public virtual ActionResult Login(LoginViewModel loginViewModel, string returnUrl)
        {
            if (ModelState.IsValid)
            {
                if (_authenticationProvider.Authenticate(loginViewModel.UserName, loginViewModel.Password))
                {
                    return Redirect(returnUrl ?? Url.Action(MVC.Admin.Index()));
                }
                else
                {
                    ModelState.AddModelError("", "Incorrect username or password");
                    return View();
                }
            }
            else
            {
                return View();
            }
        }
    }
}