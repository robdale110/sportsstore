﻿using System.Web.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using SportStore.WebUi.Controllers;
using SportStore.WebUi.Infrastructure.Abstract;
using SportStore.WebUi.Models;

namespace SportStore.Tests
{
    [TestClass]
    public class AccountControllerTests
    {
        [TestMethod]
        public void Can_Login_With_Valid_Credentials()
        {
            // Arrange
            var mock = new Mock<IAuthenticationProvider>();
            mock.Setup(m => m.Authenticate("admin", "secret")).Returns(true);
            var loginViewModel = new LoginViewModel
            {
                UserName = "admin",
                Password = "secret"
            };
            var target = new AccountController(mock.Object);

            // Act
            var result = target.Login(loginViewModel, "/MyUrl");

            // Assert
            Assert.IsInstanceOfType(result, typeof(RedirectResult));
            Assert.AreEqual("/MyUrl", ((RedirectResult)result).Url);
        }

        [TestMethod]
        public void Cannot_Login_With_Invalid_Credentials()
        {
            // Arrange
            var mock = new Mock<IAuthenticationProvider>();
            mock.Setup(m => m.Authenticate("badUser", "badPass")).Returns(false);
            var loginViewModel = new LoginViewModel
            {
                UserName = "badUser",
                Password = "badPass"
            };
            var target = new AccountController(mock.Object);

            // Act
            var result = target.Login(loginViewModel, "/MyUrl");

            // Assert
            Assert.IsInstanceOfType(result, typeof(ViewResult));
            Assert.IsFalse(((ViewResult)result).ViewData.ModelState.IsValid);
        }
    }
}
