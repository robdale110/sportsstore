﻿using System.Linq;
using System.Web;
using System.Web.Mvc;
using SportStore.Domain.Abstract;
using SportStore.Domain.Entities;

namespace SportStore.WebUi.Controllers
{
    [Authorize]
    public partial class AdminController : Controller
    {
        private IProductRepository _productRepository;

        public AdminController(IProductRepository productRepository)
        {
            _productRepository = productRepository;
        }

        public virtual ViewResult Index()
        {
            return View(_productRepository.Products);
        }

        public virtual ViewResult Create()
        {
            return View(MVC.Admin.Views.Edit, new Product());
        }

        [HttpPost]
        public virtual ActionResult Delete(int productId)
        {
            var deletedProduct = _productRepository.DeleteProduct(productId);

            if (deletedProduct != null)
            {
                TempData["message"] = string.Format("{0} was deleted", deletedProduct.Name);
            }
            return RedirectToAction(MVC.Admin.Index());
        }

        public virtual ViewResult Edit(int productId)
        {
            var product = _productRepository.Products.FirstOrDefault(p => p.ProductId == productId);

            return View(product);
        }

        [HttpPost]
        public virtual ActionResult Edit(Product product, HttpPostedFile image = null)
        {
            if (ModelState.IsValid)
            {
                if (image != null)
                {
                    product.ImageMimeType = image.ContentType;
                    product.ImageData = new byte[image.ContentLength];
                    image.InputStream.Read(product.ImageData, 0, image.ContentLength);
                }
                _productRepository.SaveProduct(product);
                TempData["message"] = string.Format("{0} has been saved", product.Name);
                return RedirectToAction(MVC.Admin.Index());
            }
            else
            {
                // Something wrong with the submitted data
                return View(product);
            }
        }
    }
}